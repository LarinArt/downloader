import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class New {

    public static void main(String[] args) {
        Download download = new Download();

        if (args.length != 3) {
            System.out.println("Отсутствуют один или несколько аргументов");
            System.exit(0);
        }
        int threads = Integer.parseInt(args[0]);  // количество потоков
        String path = args[1]; // конечная папка
        String urlList = args[2]; // файл с ссылками и именами файлов
        List<String> noduplicates = new ArrayList<>();
        List<String> links = new ArrayList<>();
        File f = new File(urlList);
        if (!f.exists() || f.isDirectory()) {
            System.out.println("Файл с ссылками отсутствует");
            System.exit(0);
        }
        try (Stream<String> stream = Files.lines(Paths.get(urlList)).distinct()) {
            String[] duplicates = stream.toArray(String[]::new);  // получаем ссылки и имена
            for (String s : duplicates) {
                String[] line = s.split(" ");
                if (!noduplicates.contains(line[0])) {    //если ссылка не дубликат добавляем в лист
                    noduplicates.add(line[0]);
                    links.add(s);
                }
            }
            if (links.isEmpty()) System.out.println("В файле отсутствуют ссылки");
            download.FileDownloader(threads, path, links);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
