import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

class Work {

    void worker(LinkedBlockingQueue<String> queue, String path, CountDownLatch countDownLatch, Stats stats) {
        String[] urlAndFilename; //разделение ссылок от имен
        String item;  //элемент из очереди
        File localFolder = new File(path);
        if (!localFolder.exists()) localFolder.mkdirs();
        try {
            while ((item = queue.poll()) != null) {
                urlAndFilename = item.split(" ");
                downloadNIO(urlAndFilename[0], path + urlAndFilename[1], urlAndFilename[1], stats);
                countDownLatch.countDown();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void downloadNIO(String fileURL, String folder, String fileName, Stats stats) throws IOException {
        URL url = new URL(fileURL);
        try (ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(folder);
             FileChannel fileChannel = fileOutputStream.getChannel()) {
            stats.dwnldStart(fileName);
            long startTime = System.currentTimeMillis();
            fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            long endTime = System.currentTimeMillis();
            stats.dwnldFinish(fileName, new File(folder).length(), (endTime - startTime));
        }
    }
}
