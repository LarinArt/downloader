interface StatCollector {

    void dwnldStart(String fileName);

    void dwnldFinish(String fileName, long fileSize, long time);
}
