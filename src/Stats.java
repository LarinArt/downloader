class Stats implements StatCollector {
    private double kBps;
    private double OverallTimeSec;
    private double OverallSizeMB;

    @Override
    public void dwnldStart(String fileName) {
        System.out.print("Загружается файл: " + fileName + "\n");
    }

    @Override
    public void dwnldFinish(String fileName, long fileSize, long time) {
        double downloadTimeSeconds = time / 1000d;
        OverallTimeSec += downloadTimeSeconds;
        double sizeMB = fileSize / 1048576d; //из байт в мегабайт
        if (downloadTimeSeconds != 0.0d) kBps += (fileSize/1024d) / downloadTimeSeconds;
        OverallSizeMB += sizeMB;

        System.out.print("Файл " + fileName + " загружен: " + Math.round(sizeMB) +
                " MB за " + Math.round(downloadTimeSeconds / 60) + " минут(ы) и " +
                Math.round(downloadTimeSeconds % 60) + " секунд(ы) " + "\n");

    }

    void printAll(int links) {
        double min = OverallTimeSec / 60;
        double sec = OverallTimeSec % 60;

        System.out.println("\n" + "Загружено: 100%" + "\n" +
                "Загружено: " + links + " файлов, " + Math.round(OverallSizeMB *10d)/10d + " MB" + "\n" +
                "Время: " + Math.round(min) + " минут(ы) " + Math.round(sec) + " секунд(ы)" + "\n" +
                "Средняя скорость: " + Math.round(kBps / links) + " kB/s");
    }


}
