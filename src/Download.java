import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;


class Download {


    void FileDownloader(int threadCount, String path, List<String> links) {
        LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>(links);
        int threadNeeded = Math.min(queue.size(), threadCount);
        CountDownLatch countDownLatch = new CountDownLatch(queue.size());
        Stats stats = new Stats();
        Work work = new Work();
        for (int i = 0; i < threadNeeded; i++) {
            Thread worker = new Thread(() -> work.worker(queue, path, countDownLatch, stats));
            worker.start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        stats.printAll(links.size());
    }
}